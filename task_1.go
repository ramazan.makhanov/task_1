package task_1

import (
	"math"
)

func ConcatString(str1 string, str2 string) (string, string){
	return str1 + str2, string(str1[0])
}

func FindRoots(b float64, c float64) (float64, float64) {

	discr := math.Pow(b,2) - 4.0*c
	d := math.Sqrt(discr)

	x1 := (-b + d) / 2.0
	x2 := (-b - d) / 2.0

	return x1, x2
}

func moveToLeft(i int) int{
	return i << 2
}
